from fastapi import APIRouter, Body, Response, Depends
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from fastapi.encoders import jsonable_encoder
from uuid import uuid4
from datetime import datetime

from app.server.middleware.deps import get_current_user

from data.models.users import (
    SystemUser
)

from data.repository.user import (
    put,
)

from data.models.users import (
    ResponseModel,
    UpdateUserSchema,
    ErrorResponseModel,
)

router = APIRouter()

@router.put("/profile", response_description="Edit Profile", status_code=200)
async def updateUser(user_params: UpdateUserSchema, response: Response, user: SystemUser = Depends(get_current_user)):
    now = datetime.now()
    formatted_date = now.strftime('%Y-%m-%d %H:%M:%S')

    user_params = jsonable_encoder(user_params)
    user_params['user_id'] = str(user.user_id)
    user_params['updated_date'] = formatted_date

    new_users = await put(user_params)

    if 'error' in new_users:
        response.status_code = 400
        return ErrorResponseModel(new_users['error'])
    
    return ResponseModel(new_users, "User added successfully.")