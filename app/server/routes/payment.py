from fastapi import APIRouter, Body, Response, Depends
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from fastapi.encoders import jsonable_encoder
from uuid import uuid4
from datetime import datetime

from app.server.middleware.deps import get_current_user

from data.models.users import (
    SystemUser
)

from data.repository.payment import (
    payments,
)

from data.models.payments import (
    ResponseModel,
    PaymentSchema,
    ErrorResponseModel,
)

router = APIRouter()

@router.post("/pay", response_description="User Topup Ewallet balance", status_code=200)
async def payment(payment_params: PaymentSchema, response: Response, user: SystemUser = Depends(get_current_user)):
    now = datetime.now()
    formatted_date = now.strftime('%Y-%m-%d %H:%M:%S')

    payment_params = jsonable_encoder(payment_params)
    payment_params['payment_id'] = str(uuid4())
    payment_params['user_id'] = str(user.user_id)
    payment_params['created_date'] = formatted_date

    new_payments = await payments(payment_params)

    if 'error' in new_payments:
        response.status_code = 400
        return ErrorResponseModel(new_payments['error'])
    
    return ResponseModel(new_payments, "User added successfully.")