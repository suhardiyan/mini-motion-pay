from fastapi import APIRouter, Body, Response, Depends, BackgroundTasks
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from fastapi.encoders import jsonable_encoder
from uuid import uuid4
from datetime import datetime
import asyncio

from app.server.middleware.deps import get_current_user

from data.models.users import (
    SystemUser
)

from data.repository.transfer import (
    transfers,
)

from data.models.transfers import (
    ResponseModel,
    TransferSchema,
    ErrorResponseModel,
)

router = APIRouter()

async def transferBg(transfer_params: TransferSchema):
    await asyncio.sleep(5)
    now = datetime.now()
    formatted_date = now.strftime('%Y-%m-%d %H:%M:%S')

    transferToUser = await transfers(transfer_params) if asyncio.iscoroutinefunction(transfers) else transfers(transfer_params)

    with open("log.txt", mode="a") as log:
        log.write("End Transfer to "+transfer_params['user_id']+" at "+formatted_date+"\n")

@router.post("/transfer", response_description="User Topup Ewallet balance", status_code=200)
async def transfer(transfer_params: TransferSchema, background_tasks: BackgroundTasks, response: Response, user: SystemUser = Depends(get_current_user)):
    now = datetime.now()
    formatted_date = now.strftime('%Y-%m-%d %H:%M:%S')

    transfer_params = jsonable_encoder(transfer_params)
    transfer_params['transfer_id'] = str(uuid4())
    transfer_params['user_id'] = str(user.user_id)
    transfer_params['created_date'] = formatted_date
    
    with open("log.txt", mode="a") as log:
        log.write("Start Transfer to "+str(user.user_id)+" at "+formatted_date+"\n")

    background_tasks.add_task(transferBg, transfer_params)

    return ResponseModel({
        "Saldo Anda sedang diproses"
    }, "")