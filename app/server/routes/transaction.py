from fastapi import APIRouter, Body, Response, Depends
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from fastapi.encoders import jsonable_encoder
from uuid import uuid4
from datetime import datetime

from app.server.middleware.deps import get_current_user

from data.models.users import (
    SystemUser
)

from data.repository.transaction import (
    transactions,
)

from data.models.transactions import (
    ResponseModel,
    ErrorResponseModel,
)

router = APIRouter()

@router.get("/transactions", response_description="Transaction History", status_code=200)
async def transaction(response: Response, user: SystemUser = Depends(get_current_user)):
    transaction_params = {}
    transaction_params['user_id'] = str(user.user_id)

    get_transaction = await transactions(transaction_params)

    if 'error' in get_transaction:
        response.status_code = 400
        return ErrorResponseModel(get_transaction['error'])
    
    return ResponseModel(get_transaction, "User added successfully.")