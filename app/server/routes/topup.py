from fastapi import APIRouter, Body, Response, Depends
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from fastapi.encoders import jsonable_encoder
from uuid import uuid4
from datetime import datetime

from app.server.middleware.deps import get_current_user

from data.models.users import (
    SystemUser
)

from data.repository.topup import (
    topups,
)

from data.models.topups import (
    ResponseModel,
    TopUpSchema,
    ErrorResponseModel,
)

router = APIRouter()

@router.post("/topup", response_description="User Topup Ewallet balance", status_code=200)
async def topup(top_ups: TopUpSchema, response: Response, user: SystemUser = Depends(get_current_user)):
    now = datetime.now()
    formatted_date = now.strftime('%Y-%m-%d %H:%M:%S')

    top_ups = jsonable_encoder(top_ups)
    top_ups['top_up_id'] = str(uuid4())
    top_ups['user_id'] = str(user.user_id)
    top_ups['created_date'] = formatted_date

    new_top_ups = await topups(top_ups)

    if 'error' in new_top_ups:
        response.status_code = 400
        return ErrorResponseModel(new_top_ups['error'])
    
    return ResponseModel(new_top_ups, "User added successfully.")