from fastapi import APIRouter, Body, Response, Depends
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from fastapi.encoders import jsonable_encoder
from uuid import uuid4
from datetime import datetime, timedelta, timezone
from jose import JWTError, jwt
from passlib.context import CryptContext
from typing import Annotated, Union
from app.config import (
    ACCESS_TOKEN_EXPIRE_MINUTES,
    SECRET_KEY,
    ALGORITHM,
    REFRESH_KEY
)
from app.server.middleware.deps import get_current_user

from data.repository.user import (
    registers,
    find_user_by_phone
)
from data.models.users import (
    ResponseModel,
    UserSchema,
    UserLoginSchema,
    ErrorResponseModel,
    Token,
    TokenData,
    UserInDB,
    TokenPayload,
    SystemUser,
    UserOut
)

router = APIRouter()

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")

def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password):
    return pwd_context.hash(password)


def get_user(db, username: str):
    if username in db:
        user_dict = db[username]
        return UserInDB(**user_dict)
    
def authenticate_user(username: str, pin: str):
    user = get_user(username)
    if not user:
        return False
    if not verify_password(pin, user.hashed_password):
        return False
    return user

def create_access_token(subject: Union[str, any], expires_delta: int = None) -> str:
    if expires_delta is not None:
        expires_delta = datetime.utcnow() + expires_delta
    else:
        expires_delta = datetime.utcnow() + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    
    to_encode = {"exp": expires_delta, "sub": str(subject)}
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, ALGORITHM)
    return encoded_jwt

def create_refresh_token(subject: Union[str, any], expires_delta: int = None) -> str:
    if expires_delta is not None:
        expires_delta = datetime.utcnow() + expires_delta
    else:
        expires_delta = datetime.utcnow() + timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    
    to_encode = {"exp": expires_delta, "sub": str(subject)}
    encoded_jwt = jwt.encode(to_encode, REFRESH_KEY, ALGORITHM)
    return encoded_jwt


@router.post("/register", response_description="User data added into the database", status_code=200)
async def register(users: UserSchema, response: Response):
    now = datetime.now()
    formatted_date = now.strftime('%Y-%m-%d %H:%M:%S')

    users = jsonable_encoder(users)
    users['user_id'] = str(uuid4())
    users['created_date'] = formatted_date
    users['pin'] = get_password_hash(users['pin'])
    users['balance'] = 0

    new_users = await registers(users)

    if 'error' in new_users:
        response.status_code = 400
        return ErrorResponseModel(new_users['error'])
    
    return ResponseModel(new_users, "User added successfully.")

@router.post("/login", response_description="User login into apps", status_code=200)
async def login(users: UserLoginSchema, response: Response):
    users = jsonable_encoder(users)
    user = await find_user_by_phone(users)
    if 'error' in user:
        response.status_code = 401
        return ErrorResponseModel(user['error'])
    
    hashed_pass = user['pin']
    if not verify_password(users['pin'], hashed_pass):
        response.status_code = 401
        return ErrorResponseModel("Phone number and pin doesn't match.")
    
    return ResponseModel({
        "access_token": create_access_token(user['phone_number']),
        "refresh_token": create_refresh_token(user['phone_number']),
    }, "User added successfully.")

@router.get('/me', summary='Get details of currently logged in user', response_model=UserOut)
async def get_me(user: SystemUser = Depends(get_current_user)):
    return user