import sys
sys.path.insert(0, '/Users/admin/Documents/Project/python/minimotionpay')

from fastapi import FastAPI

from app.server.routes.auth import router as AuthRouter
from app.server.routes.topup import router as TopupRouter
from app.server.routes.payment import router as PaymentRouter
from app.server.routes.transfer import router as TransferRouter
from app.server.routes.transaction import router as TransactionRouter
from app.server.routes.user import router as UserRouter

app = FastAPI()

app.include_router(AuthRouter, tags=["Auth"])
app.include_router(TopupRouter, tags=["Topup"])
app.include_router(PaymentRouter, tags=["Payment"])
app.include_router(TransferRouter, tags=["Transfer"])
app.include_router(TransactionRouter, tags=["Transaction"])
app.include_router(UserRouter, tags=["User"])

@app.get("/", tags=["Root"])
async def read_root():
    return {"message": "Welcome to this fantastic app!"}