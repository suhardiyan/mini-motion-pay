from bson.objectid import ObjectId
import motor.motor_asyncio
from datetime import datetime

MONGO_DETAILS = "mongodb://localhost:27017"

client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_DETAILS)

database = client.minimotionpay

payment_collection = database.get_collection("payment_collection")
top_up_collection = database.get_collection("top_up_collection")
transfer_collection = database.get_collection("transfer_collection")
users_collection = database.get_collection("users_collection")

async def transactions(transaction_data: dict) -> dict:
    payments = await payment_collection.find({"user_id": str(transaction_data['user_id'])}).to_list(None)
    topups = await top_up_collection.find({"user_id": str(transaction_data['user_id'])}).to_list(None)
    transfers_origin = await transfer_collection.find({"origin_user_id": str(transaction_data['user_id'])}).to_list(None)
    transfers_dest = await transfer_collection.find({"dest_user_id": str(transaction_data['user_id'])}).to_list(None)

    all_transactions = (format_payments(payments) +
                        format_topups(topups) +
                        format_transfers(transfers_origin, "DEBIT") +
                        format_transfers(transfers_dest, "CREDIT"))

    all_transactions.sort(key=lambda x: datetime.strptime(x["created_date"], "%Y-%m-%d %H:%M:%S"))

    current_balance = 0
    for transaction in all_transactions:
        transaction["balance_before"] = current_balance
        if transaction["transaction_type"] == "DEBIT":
            current_balance -= float(transaction["amount"])
        else:
            current_balance += float(transaction["amount"])
        transaction["balance_after"] = current_balance

    return all_transactions

def format_payments(payments):
    return [{
        "payment_id": p["payment_id"],
        "transaction_type": "DEBIT",
        "amount": float(p["amount"]),
        "created_date": p["created_date"],
        "remarks": p.get("remarks", "")
    } for p in payments]

def format_topups(topups):
    return [{
        "top_up_id": t["top_up_id"],
        "transaction_type": "CREDIT",
        "amount": float(t["amount"]),
        "created_date": t["created_date"],
        "remarks": t.get("remarks", "")
    } for t in topups]

def format_transfers(transfers, transaction_type):
    return [{
        "transfer_id": tr["transfer_id"],
        "transaction_type": transaction_type,
        "amount": float(tr["amount"]),
        "created_date": tr["created_date"],
        "remarks": tr.get("remarks", "")
    } for tr in transfers]