from bson.objectid import ObjectId
import motor.motor_asyncio

MONGO_DETAILS = "mongodb://localhost:27017"

client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_DETAILS)

database = client.minimotionpay

payment_collection = database.get_collection("payment_collection")
users_collection = database.get_collection("users_collection")

def payment_helper(payment) -> dict:
    return {
        "payment_id": payment["payment_id"],
        "amount": payment["amount"],
        "remarks": payment["remarks"],
        "balance_before": payment["balance_before"],
        "balance_after": payment["balance_after"],
        "created_date": payment["created_date"]
    }

async def payments(payment_data: dict) -> dict:
    user = await users_collection.find_one({"user_id": str(payment_data['user_id'])})

    if not user:
        return {"error": "User not found"}

    balance_before = float(user['balance'])
    print("Balance before:", balance_before)

    balance_after = balance_before - float(payment_data['amount'])

    if balance_after < 0:
        return {"error": "Balance is not enough"}
    
    payment = await payment_collection.insert_one(payment_data)
    print("Balance after:", balance_after)

    await users_collection.update_one(
        {"user_id": str(payment_data['user_id'])},
        {"$set": {"balance": balance_after}}
    )

    new_payment = await payment_collection.find_one({"_id": payment.inserted_id})

    if new_payment:
        new_payment['balance_before'] = balance_before
        new_payment['balance_after'] = balance_after
        new_payment['amount'] = float(new_payment['amount'])
    else:
        return {"error": "Top up record not found"}

    return payment_helper(new_payment)