from bson.objectid import ObjectId
import motor.motor_asyncio

MONGO_DETAILS = "mongodb://localhost:27017"

client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_DETAILS)

database = client.minimotionpay

users_collection = database.get_collection("users_collection")

def users_helper(users) -> dict:
    return {
        "id": str(users["_id"]),
        "first_name": users["first_name"],
        "last_name": users["last_name"],
        "phone_number": users["phone_number"],
        "pin": users["pin"],
        "address": users["address"],
        "user_id": users["user_id"],
        "created_date": users["created_date"],
    }

async def put(user_data: dict) -> dict:
    update_query = {"$set": user_data}
    
    updated_user = await users_collection.find_one_and_update(
        {"user_id": user_data.get('user_id')},
        update_query,
        return_document=True
    )
    
    return users_helper(updated_user)

async def registers(user_data: dict) -> dict:
    existing_user = await users_collection.find_one({"phone_number": user_data.get('phone_number')})
    
    if existing_user:
        return {"error": "Phone number already registered"}
    
    user = await users_collection.insert_one(user_data)
    new_user = await users_collection.find_one({"_id": user.inserted_id})
    return users_helper(new_user)

async def find_user_by_phone(user_data: dict) -> dict:
    existing_user = await users_collection.find_one({"phone_number": user_data.get('phone_number')})
    
    if not existing_user:
        return {"error": "Phone number and pin doesn't match."}
    
    return users_helper(existing_user)

async def find_user_by_phone_number(phone_number: str) -> str:
    existing_user = await users_collection.find_one({"phone_number": phone_number})
    
    if not existing_user:
        return {"error": "Phone number and pin doesn't match."}
    
    return users_helper(existing_user)