from bson.objectid import ObjectId
import motor.motor_asyncio

MONGO_DETAILS = "mongodb://localhost:27017"

client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_DETAILS)

database = client.minimotionpay

transfer_collection = database.get_collection("transfer_collection")
users_collection = database.get_collection("users_collection")

def transfer_helper(transfer) -> dict:
    return {
        "transfer_id": transfer["transfer_id"],
        "amount": transfer["amount"],
        "remarks": transfer["remarks"],
        "balance_before": transfer["balance_before"],
        "balance_after": transfer["balance_after"],
        "created_date": transfer["created_date"]
    }

async def transfers(transfer_data: dict) -> dict:
    origin_user = await users_collection.find_one({"user_id": str(transfer_data['user_id'])})
    dest_user = await users_collection.find_one({"user_id": str(transfer_data['target_user'])})

    if not origin_user:
        return {"error": "User not found"}

    if 'balance' not in origin_user:
        origin_user['balance'] = 0
    if 'balance' not in dest_user:
        dest_user['balance'] = 0

    origin_balance_before = float(origin_user['balance'])
    dest_balance_before = float(dest_user['balance'])

    print("Balance before:", origin_balance_before)

    origin_balance_after = origin_balance_before - float(transfer_data['amount'])
    dest_balance_after = dest_balance_before + float(transfer_data['amount'])

    if origin_balance_after < 0:
        return {"error": "Balance is not enough"}
    
    transfer = await transfer_collection.insert_one(transfer_data)
    print("Balance after:", origin_balance_after)

    await users_collection.update_one(
        {"user_id": str(transfer_data['user_id'])},
        {"$set": {"balance": origin_balance_after}}
    )

    await users_collection.update_one(
        {"user_id": str(transfer_data['target_user'])},
        {"$set": {"balance": dest_balance_after}}
    )

    new_transfer = await transfer_collection.find_one({"_id": transfer.inserted_id})

    if new_transfer:
        new_transfer['balance_before'] = origin_balance_before
        new_transfer['balance_after'] = origin_balance_after
        new_transfer['amount'] = float(new_transfer['amount'])
    else:
        return {"error": "Top up record not found"}

    return transfer_helper(new_transfer)