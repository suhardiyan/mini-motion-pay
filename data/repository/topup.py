from bson.objectid import ObjectId
import motor.motor_asyncio

MONGO_DETAILS = "mongodb://localhost:27017"

client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_DETAILS)

database = client.minimotionpay

top_up_collection = database.get_collection("top_up_collection")
users_collection = database.get_collection("users_collection")

def top_up_helper(top_up) -> dict:
    return {
        "top_up_id": top_up["top_up_id"],
        "amount_top_up": top_up["amount"],
        "balance_before": top_up["balance_before"],
        "balance_after": top_up["balance_after"],
        "created_date": top_up["created_date"]
    }

async def topups(topup_data: dict) -> dict:
    user = await users_collection.find_one({"user_id": str(topup_data['user_id'])})

    if not user:
        return {"error": "User not found"}

    balance_before = float(user['balance'])
    print("Balance before:", balance_before)

    topup = await top_up_collection.insert_one(topup_data)
    balance_after = balance_before + float(topup_data['amount'])
    print("Balance after:", balance_after)

    await users_collection.update_one(
        {"user_id": str(topup_data['user_id'])},
        {"$set": {"balance": balance_after}}
    )

    new_topup = await top_up_collection.find_one({"_id": topup.inserted_id})

    if new_topup:
        new_topup['balance_before'] = balance_before
        new_topup['balance_after'] = balance_after
        new_topup['amount_top_up'] = float(new_topup['amount_top_up'])
    else:
        return {"error": "Top up record not found"}

    return top_up_helper(new_topup)