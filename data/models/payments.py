from pydantic import BaseModel, Field

class PaymentSchema(BaseModel):
    amount: str = Field(...)
    remarks: str = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "amount": "50000",
                "remarks": "Pulsa Telkomsel 100k",
            }
        }

def ResponseModel(data, message):
    return {
        "status": "SUCCESS",
        "result": data,
        "code": 200,
    }

def ErrorResponseModel(message):
    return {
        "message": message
    }
