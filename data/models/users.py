from typing import Optional, Union
from uuid import UUID
from pydantic import BaseModel, EmailStr, Field

class UpdateUserSchema(BaseModel):
    first_name: str = Field(...)
    last_name: str = Field(...)
    address: str = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "first_name": "Guntur",
                "last_name": "Saputro",
                "address": "Jl.Kebon Sirih No. 1",
            }
        }

class UserSchema(BaseModel):
    first_name: str = Field(...)
    last_name: str = Field(...)
    phone_number: str = Field(...)
    address: str = Field(...)
    pin: str = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "first_name": "Guntur",
                "last_name": "Saputro",
                "phone_number": "0811255501",
                "address": "Jl.Kebon Sirih No. 1",
                "pin": "123456"
            }
        }

class UserLoginSchema(BaseModel):
    phone_number: str = Field(...)
    pin: str = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "phone_number": "0811255501",
                "pin": "123456"
            }
        }

class Token(BaseModel):
    access_token: str
    refresh_token: str
    token_type: str

class TokenData(BaseModel):
    username: Union[str, None] = None

class UserInDB(UserSchema):
    hashed_password: str

class TokenPayload(BaseModel):
    sub: str = None
    exp: int = None

class UserOut(BaseModel):
    user_id: UUID
    phone_number: str

class SystemUser(UserOut):
    pin: str
    id: str

def ResponseModel(data, message):
    return {
        "status": "SUCCESS",
        "result": data,
        "code": 200,
    }

def ErrorResponseModel(message):
    return {
        "message": message
    }
