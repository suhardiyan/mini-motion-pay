from pydantic import BaseModel, Field

class TopUpSchema(BaseModel):
    amount: str = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "amount": "50000",
            }
        }

def ResponseModel(data, message):
    return {
        "status": "SUCCESS",
        "result": data,
        "code": 200,
    }

def ErrorResponseModel(message):
    return {
        "message": message
    }
