from pydantic import BaseModel, Field

class TransactionSchema(BaseModel):
    target_user: str = Field(...)
    amount: str = Field(...)
    remarks: str = Field(...)

    class Config:
        schema_extra = {
            "example": {
                "target_user": "“a7d39cf6-44b6-41fcb3e9-7b16df5321c5",
                "amount": "50000",
                "remarks": "Pulsa Telkomsel 100k",
            }
        }

def ResponseModel(data, message):
    return {
        "status": "SUCCESS",
        "result": data,
        "code": 200,
    }

def ErrorResponseModel(message):
    return {
        "message": message
    }
